<?php


/**
 * Implements hook_rules_action_info().
 */
function commerce_billy_cancel_rules_action_info() {
  return [
    'commerce_billy_cancel_nr' => [
      'label' => t('Set cancel number on order'),
      'parameter' => [
        'order' => ['type' => 'commerce_order', 'label' => t('Order')],
      ],
      'group' => t('Commerce Billy Cancel'),
    ],
  ];
}

<?php

/**
 * Implements hook_token_info_alter().
 */
function commerce_billy_cancel_token_info_alter(&$data) {
  $data['tokens']['commerce-order']['cancel-number'] = [
    'name' => 'Cancel number',
    'description' => 'The cancel number displayed to the customer',
  ];
}


function commerce_billy_cancel_tokens($type, $tokens, array $data = [], array $options = []) {
  $sanitize = !empty($options['sanitize']);

  $replacements = [];

  if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
    $order = $data['commerce-order'];    

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'cancel-number':
          $replacements[$original] = $sanitize ? check_plain($order->cancel_number) : $order->cancel_number;
          break;
      }
    }
  }

  return $replacements;
}


<?php
/**
 * @file
 * commerce_billy.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_billy_cancel_default_rules_configuration() {
  $items = [];

  $items['rules_commerce_billy_generate_cancel_number_on_order_update'] = entity_import('rules_config', '{ "rules_commerce_billy_generate_cancel_number_on_order_update" : {
      "LABEL" : "Generate cancel number on order update",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Commerce Billy Cancel" ],
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_order_update" ],
      "IF" : [
        { "NOT data_is" : { "data" : [ "commerce-order-unchanged:status" ], "value" : "canceled" } },
        { "data_is" : { "data" : [ "commerce-order:status" ], "value" : "canceled" } }
      ],
      "DO" : [
        { "component_rules_commerce_billy_cancel_set_to_canceled" : { "order" : [ "commerce-order" ] } }
      ]
    }
  }');


  $items['rules_commerce_billy_cancel_set_to_canceled'] = entity_import('rules_config', '{ "rules_commerce_billy_cancel_set_to_canceled" : {
      "LABEL" : "Set order to canceled",
      "PLUGIN" : "rule",
      "TAGS" : [ "Commerce Billy Cancel" ],
      "REQUIRES" : [ "rules", "commerce_order", "commerce_billy", "commerce_billy_cancel" ],
      "USES VARIABLES" : { "order" : { "label" : "Order", "type" : "commerce_order" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "order" ], "field" : "field_commerce_billy_cancel_date" } },
        { "NOT data_is" : { "data" : [ "order:order-number" ], "value" : [ "order:order-id" ] } },
        { "data_is_empty" : { "data" : [ "order:cancel-number" ] } }
      ],
      "DO" : [
        { "commerce_order_update_state" : { "commerce_order" : [ "order" ], "order_state" : "canceled" } },
        { "commerce_billy_cancel_nr" : { "order" : [ "order" ] } }        
      ]
    }
  }');
  return $items;
}
